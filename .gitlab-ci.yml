stages:
  - Check
  - Prepare
  - Simulation
  - Install
  - Tool panel
  - Sync


########## Templates Environments ##########

.check:
  tags:
    - docker
  except:
    - schedules
  image: registry.gitlab.com/ifb-elixirfr/docker-images/docker-with-molecule

.dev:
  only:
    - dev
  tags:
    - igbmc
    - shell
  variables:
    # Define variable REQUESTS_CA_BUNDLE used by python library requests
    # This lib is used by pip and galaxy tools
    # Used to accept self-signed certificate
    REQUESTS_CA_BUNDLE: "/tmp/certs.pem"

.preprod:
  only:
    - master
  except:
    - schedules
  tags:
    - igbmc
    - shell
  variables:
    # Define variable REQUESTS_CA_BUNDLE used by python library requests
    # This lib is used by pip and galaxy tools
    # Used to accept self-signed certificate
    REQUESTS_CA_BUNDLE: "/tmp/certs.pem"

.prod:
  only:
    - master
  except:
    - schedules
  tags:
    - idris
    - shell

########## Check ##########

Yaml:
 extends: .check
 stage: Check
 before_script:
    - pip install -r requirements.txt
 script: "sh ./scripts/lint_all.sh"

Playbook (dev):
 extends: .check
 stage: Check
 script: "ansible-playbook -vvv -i development --syntax-check playbook_*.yml"

Playbook (preprod):
  extends: .check
  stage: Check
  script: "ansible-playbook -i preproduction --syntax-check playbook_*.yml"

Playbook (prod):
  extends: .check
  stage: Check
  script: "ansible-playbook -i production --syntax-check playbook_*.yml"

########## Get SSL certificate ##########

SSL cert (dev):
  extends:
    - .dev
  stage: Prepare
  script: "ansible-playbook -i development playbook_get_SSL_certs.yml"

SSL cert (preprod):
  extends:
    - .preprod
  stage: Prepare
  script: "ansible-playbook -i preproduction playbook_get_SSL_certs.yml"

########## Simulation ##########

Check (dev):
  extends: .dev
  stage: Simulation
  script: "ansible-playbook -i development --extra-vars \"galaxy_tools_api_key=$GALAXY_API_KEY_DEV\" --check --diff playbook_galaxy_tools.yml"
  when: manual
  only:
    - dev
  except:
    - schedules

Check (preprod):
  stage: Simulation
  script: "ansible-playbook -i preproduction --extra-vars \"galaxy_tools_api_key=$GALAXY_API_KEY_PREPROD\" --check --diff playbook_galaxy_tools.yml"
  when: manual
  only:
    - preprod
  except:
    - schedules
  tags:
    - igbmc

Check (prod):
  stage: Simulation
  script: "ansible-playbook -i production --extra-vars \"galaxy_tools_api_key=$GALAXY_API_KEY_PROD\" --check --diff playbook_galaxy_tools.yml"
  when: manual
  only:
    - master
  except:
    - schedules
  tags:
    - idris


########## Apps ##########

Sample tools (dev):
  stage: Install
  extends:
    - .dev
  script: 
    - |
      ansible-playbook -i development playbook_galaxy_tools.yml --diff --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_DEV" --extra-vars "TOOL_LISTS=files/get_data.yml.lock,files/send_data.yml.lock,files/collection_operations.yml.lock,files/proteore.yml.lock,files/galaxyP.yml.lock,files/graphdisplay_data.yml.lock,files/met4j.yml"

All tools (preprod):
  stage: Install
  extends:
    - .preprod
  script:
    - |
      ansible-playbook -i preproduction playbook_galaxy_tools.yml --diff --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_PREPROD" --extra-vars "TOOL_LISTS=$(ls files/*lock | tr '\n' ',' | sed 's/,$/\n/')"

All tools (prod):
  stage: Install
  extends:
    - .prod
  script:
    - |
      ansible-playbook -i production playbook_galaxy_tools.yml --diff --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_PROD" --extra-vars "TOOL_LISTS=$(ls files/*lock | tr '\n' ',' | sed 's/,$/\n/')"


########## Tool panel ##########

Panel update (dev):
  extends:
    - .dev
  stage: Tool panel
  script:
    - |
      ansible-playbook -i development --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_DEV" playbook_galaxy_sort_integrated_tool_panel.yml

Panel update (preprod):
  extends:
    - .preprod
  stage: Tool panel
  script:
    - |
      ansible-playbook -i preproduction --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_PREPROD" playbook_galaxy_sort_integrated_tool_panel.yml

Panel update (prod):
  extends:
    - .prod
  stage: Tool panel
  script:
    - |
      ansible-playbook -i production --extra-vars "galaxy_tools_api_key=$GALAXY_API_KEY_PROD" playbook_galaxy_sort_integrated_tool_panel.yml


# Scheduled builds means we need to sync tool lists and create a merge request
tool_sync:
  image:
    name: python:3.10-slim-bullseye
    # workaround for "shell not found" error https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27614
    entrypoint: [ '/bin/bash', '-c', 'ln -snf /bin/bash /bin/sh && /bin/bash -c $0' ]
  tags:
    - igbmc
    - docker
  only:
    - schedules
  stage: Sync
  before_script:
    - apt-get update && apt-get install -y git gettext jq curl patch python3-lxml && rm -rf /var/lib/apt/lists/*
    - pip install -r requirements_ephemeris.txt
    - pip install --no-deps git+https://github.com/galaxyproject/ephemeris.git@master
    - git config --global user.email 'ifbot@france-bioinformatique.fr'
    - git config --global user.name 'Usegalaxy.fr bot'
  script:
    - bash ./scripts/lint_all.sh
    - bash ./scripts/sync_all.sh
    - bash ./scripts/create_mr.sh
